<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APIFailTest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        //Missing Fields
        $payload = array();
        $result = json_decode($this->api->httpPost($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);

        //Invalid Payload
        $payload = null;
        $result = json_decode($this->api->httpPost($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);

    }

    public function testHttpGet()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        //Invalid Payload
        $payload = 'error_test';
        $result = json_decode($this->api->httpGet($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);

    }

    public function testHttpPut()
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';

        //Missing Fields
        $id = 8;
        $payload = array();
        $result = json_decode($this->api->httpPut($id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);

        //Mismatched ID
        $id = 8;
        $payload = array(
            'id' => 10,
            'first_name' => 'FailTest',
            'last_name' => 'FailTest',
        );
        $result = json_decode($this->api->httpPut($id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);

        //Non-existent ID
        $id = 999;
        $payload = array('id' => $id);
        $result = json_decode($this->api->httpPut($id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);
    }

    public function testHttpDelete()
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        //Non-existent ID
        $id = 999;
        $payload = array('id' => $id);
        $result = json_decode($this->api->httpDelete($id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);

        //Mismatched ID
        $mismatched_id = 69;
        $payload = array('id' => 99);
        $result = json_decode($this->api->httpDelete($mismatched_id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);

        //Missing Fields
        $payload = array();
        $result = json_decode($this->api->httpPost($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'fail');
        $this->assertArrayHasKey('message', $result);
    }
}
?>