<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APISuccessTest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }

    // Test HTTP POST Request
    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        //randomizing data with prefix Test
        $fname = 'Test' . rand(0, 1000);
        $lname = 'Last' . rand(0, 1000);
        $mname = 'Test' . rand(0, 1000);
        $contact = rand(0, 10000000);
        $payload = array(
            'first_name' => $fname,
            'last_name' => $lname,
            'middle_name' => $mname,
            'contact_number' => $contact
        );

        $result = json_decode($this->api->httpPost($payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);

        return $result;
    }

    // Test HTTP GET Request
    /**
     * @depends testHttpPost
     */
    public function testHttpGet($resultFromHttpPost)
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $id = $resultFromHttpPost['data']['id'];

        //Get by ID
        $idPayload = array(
            'id' => $id,
        );
        $resultById = json_decode($this->api->httpGet($idPayload), true);
        $this->assertArrayHasKey('status', $resultById);
        $this->assertEquals($resultById['status'], 'success');
        $this->assertArrayHasKey('data', $resultById);

        //Get by first_name
        $firstNamePayload = array(
            'first_name' => 'Test',
        );
        $resultByFirstName = json_decode($this->api->httpGet($firstNamePayload), true);
        $this->assertArrayHasKey('status', $resultByFirstName);
        $this->assertEquals($resultByFirstName['status'], 'success');
        $this->assertArrayHasKey('data', $resultByFirstName);

        //Get all employees
        $allDataPayload = array();
        $resultAllData = json_decode($this->api->httpGet($allDataPayload), true);
        $this->assertArrayHasKey('status', $resultAllData);
        $this->assertEquals($resultAllData['status'], 'success');
        $this->assertArrayHasKey('data', $resultAllData);
    }

    /**
     * @depends testHttpPost
     */
    public function testHttpPut($resultFromHttpPost)
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';

        $id = $resultFromHttpPost['data']['id'];

        //randomizing data with prefix Edited
        $fname = 'Edited' . rand(0, 1000);
        $lname = 'Edited' . rand(0, 1000);
        $mname = 'Edited' . rand(0, 1000);
        $contact = rand(0, 10000000);

        $payload = array(
            'id' => $id,
            'first_name' => $fname,
            'last_name' => $lname,
            'middle_name' => $mname,
            'contact_number' => $contact
        );

        $result = json_decode($this->api->httpPut($id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }
    /**
     * @depends testHttpPost
     */
    public function testHttpDelete($resultFromHttpPost)
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $id = $resultFromHttpPost['data']['id'];
        $payload = array(
            'id' => $id,
        );

        $result = json_decode($this->api->httpDelete($id, $payload), true);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }
}
?>