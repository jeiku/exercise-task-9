<?php
/**
 * Tells the browser to allow code from any origin to access
 */
header("Access-Control-Allow-Origin: *");
/**
 * Tells browsers whether to expose the response to the frontend JavaScript code
 * when the request's credentials mode (Request.credentials) is include
 */
header("Access-Control-Allow-Credentials: true");
/**
 * Specifies one or more methods allowed when accessing a resource in response to a preflight request
 */
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
/**
 * Used in response to a preflight request which includes the Access-Control-Request-Headers to
 * indicate which HTTP headers can be used during the actual request
 */
header("Access-Control-Allow-Headers: Content-Type");

//require_once('MysqliDb.php');

class API
{
    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'employee');
    }

    // Error function Response
    private function errorResponse($method, $message)
    {
        return json_encode(
            array(
                'method' => $method,
                'status' => 'fail',
                'message' => $message
            )
        );
    }
    // Success function Response
    private function successResponse($method, $data = null)
    {
        return json_encode(
            array(
                'method' => $method,
                'status' => 'success',
                'data' => $data
            )
        );
    }
    // Missing Fields function Response
    private function checkMissingFields($payload, $requiredFields)
    {
        $missingFields = array_diff($requiredFields, array_keys($payload));
        if (!empty($missingFields)) {
            return json_encode([
                'status' => 'fail',
                'message' => 'Required Fields not fill',
                'missing_fields' => $missingFields
            ]);
        }
        return null;
    }

    /**
     * HTTP GET Request
     *
     * @param $payload
     * @return string
     */
    public function httpGet($payload)
    {
        if (!is_array($payload)) {
            return $this->errorResponse('GET', 'Required fields not fill');
        }

        if (isset($payload['id'])) {
            $result = $this->db->where('id', $payload['id'])->getOne('information');
        } elseif (isset($payload['first_name'])) {
            $result = $this->db->where('first_name', $payload['first_name'])->get('information');
        } else {
            $result = $this->db->get('information');
        }

        return $result ? $this->successResponse('GET', $result) : $this->errorResponse('GET', 'Failed Fetch Request');

    }

    /**
     * HTTP POST Request
     *
     * @param $payload
     * @return string
     */
    public function httpPost($payload)
    {
        if (!is_array($payload) || empty($payload)) {
            return $this->errorResponse('POST', 'Invalid Parameters');
        }

        $requiredFields = ['first_name', 'last_name'];
        $missingFieldsResponse = $this->checkMissingFields($payload, $requiredFields);
        if ($missingFieldsResponse !== null) {
            return $missingFieldsResponse;
        }

        $id = $this->db->insert('information', $payload);

        return $id ? $this->successResponse('POST', array_merge($payload, ['id' => $id])) : $this->errorResponse('POST', 'Failed to Insert Data');
    }

    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     * @return string
     */
    public function httpPut($id, $payload)
    {
        if (empty($id) || empty($payload)) {
            return $this->errorResponse('PUT', 'Invalid Parameters');
        }

        $requiredFields = ['id', 'first_name', 'last_name'];
        $missingFieldsResponse = $this->checkMissingFields($payload, $requiredFields);

        if ($missingFieldsResponse !== null) {
            return $missingFieldsResponse;
        }

        if ($id != $payload['id']) {
            return $this->errorResponse('PUT', 'ID does not match with the payload');
        }

        $existingData = $this->db->where('id', $id)->getOne('information');
        if (!$existingData) {
            return $this->errorResponse('PUT', 'ID does not exist in the database');
        }

        $this->db->where('id', $id);
        $result = $this->db->update('information', $payload);

        return $result ? $this->successResponse('PUT', $payload) : $this->errorResponse('PUT', 'Failed to Update Data');
    }

    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     * @return string
     */
    public function httpDelete($id, $payload)
    {
        $requiredFields = ['id'];
        $missingFieldsResponse = $this->checkMissingFields($payload, $requiredFields);

        if ($missingFieldsResponse !== null) {
            return $missingFieldsResponse;
        }

        if ($id != $payload['id']) {
            return $this->errorResponse('DELETE', 'ID does not match with the payload');
        }

        $existingData = $this->db->where('id', $id)->orWhere('id', $payload['id'])->get('information');
        if (count($existingData) < 1) {
            return $this->errorResponse('DELETE', 'ID or Payload ID does not exist in the database');
        }

        $result = $this->db->where('id', $payload['id'])->delete('information');

        return $result
            ? $this->successResponse('DELETE', [])
            : $this->errorResponse('DELETE', 'Failed to Delete Data');
    }
}

//Identifier if what type of request
// $request_method = $_SERVER['REQUEST_METHOD'];

// // For GET,POST,PUT & DELETE Request
// if ($request_method === 'GET') {
//     $received_data = $_GET;
// } else {
//     //check if method is PUT or DELETE, and get the ids on URL
//     if ($request_method === 'PUT' || $request_method === 'DELETE') {
//         $request_uri = $_SERVER['REQUEST_URI'];

//         $ids = null;
//         $exploded_request_uri = array_values(explode("/", $request_uri));

//         $last_index = count($exploded_request_uri) - 1;

//         $ids = $exploded_request_uri[$last_index];


//     }
// }

// //payload data
// $received_data = json_decode(file_get_contents('php://input'), true);

// $api = new API;

// //Checking if what type of request and designating to specific functions
// switch ($_SERVER['REQUEST_METHOD']) {
//     case 'GET':
//         echo $api->httpGet($received_data);
//         break;
//     case 'POST':
//         echo $api->httpPost($received_data);
//         break;
//     case 'PUT':
//         echo $api->httpPut($ids, $received_data);
//         break;
//     case 'DELETE':
//         echo $api->httpDelete($ids, $received_data);
//         break;
// }
?>